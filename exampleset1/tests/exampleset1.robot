*** Settings ***

Suite Setup     Test suite setup
Suite Teardown  Test suite teardown
Test Setup      setup
Test Teardown   teardown

Library    QAutoLibrary.QAutoRobot
Resource   ../resources/demo_variables.robot

*** Variables ***

${url}    http://demo.qautomate.fi

*** Test Cases ***
Test example1

    Open application url    ${url}
    Input username login    ${login j_username}
    Input password login    ${login j_password}
    Sign in

*** Keywords ***
setup
    log  setup
    Start recording  ${TEST NAME}

teardown
    ${failure_image_path}=  Get failure image path  ${TEST NAME}
    Run Keyword If Test Failed  Take full screenshot  ${failure_image_path}

    Stop recording
    
    ${documentation}=  Generate failure documentation  ${TEST_DOCUMENTATION}  ${TEST NAME}
    Run Keyword If Test Failed  Set test documentation  ${documentation}
    
Test suite setup
    ${DefaultBrowser}=  Open browser  ${BROWSER}
    Set suite variable  ${DefaultBrowser}  ${DefaultBrowser}

Test suite teardown
    Close all browsers


